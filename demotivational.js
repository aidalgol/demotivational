// Aidan's demotivational-poster generator
// Copyright (C) 2011  Aidan Gauland

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

function generate_poster()
{
  var canvas = document.getElementById("poster");
  var ctx = canvas.getContext("2d");

  var image = new Image();
  {
    var image_url = document.getElementById("image_url");
    var image_file = document.getElementById("image_file");
    if (!image_url.hasAttribute("disabled"))
      image.src = image_url.value;
    else if (!image_file.hasAttribute("disabled")) {
      image.src = URL.createObjectURL(image_file.files[0]);
    }
    else
      alert("The UI is in an invalid state.\nNo idea how that happened.");
  }

  image.onload = function() {
    var poster_padding = 40; // Padding around the contents of the poster.
    var border_padding = 5; // Padding between the image and its border.
    var caption_padding = 15; // Padding between the image and the caption.
    var subcaption_padding = 10; // Padding between the caption and subcaption.
    var subcaption_line_padding = 5; // Space between subcaption lines.

    function caption_font(size)
    {
      return "bold " + size + "pt FreeSerif, Georgia, LiberationSerif, serif";
    }

    function subcaption_font(size)
    {
      return size + "pt sans-serif";
    }

    /* Compute object sizes before any drawing because we need them to
     * compute the appropriate dimensions of the canvas. */
    var caption = document.getElementById("caption").value;
    var caption_size = parseInt(document.getElementById("caption_size").value);
    ctx.font = caption_font(caption_size);
    canvas.width =
      Math.max(image.width, ctx.measureText(caption).width) +
      2*poster_padding;
    var subcaption = document.getElementById("subcaption").value;
    var subcaption_size = parseInt(document.getElementById("subcaption_size").value);
    var subcaption_lines = [];

    ctx.font = subcaption_font(subcaption_size);
    for (var words = subcaption.split(" "),
         line = "", i = 0;
         i < words.length; i++) {
      line += (words[i] + " ");
      if (ctx.measureText(line + " " + words[i+1]).width >
          canvas.width - poster_padding) {
        subcaption_lines.push(line);
        line = "";
      }
    }
    /* Push any words that were leftover because the last line was
     * not longer than the poster's width. */
    if (line != "")
      subcaption_lines.push(line);

    canvas.height =
      image.height + caption_size + subcaption_size +
      border_padding + 2*poster_padding + caption_padding + subcaption_padding +
      subcaption_size*(subcaption_lines.length-1) + subcaption_line_padding*subcaption_lines.length;

    // Give the canvas a black background.
    ctx.fillStyle = "black";
    ctx.fillRect(0, 0, canvas.width, canvas.height);

    // Draw the image horizontally centered and slightly offset vertically.
    var image_x = (canvas.width-image.width)/2;
    var image_y = poster_padding;
    ctx.drawImage(image, image_x, image_y);

    // Draw a white border outset a few pixels from the image.
    ctx.strokeStyle = "white";
    ctx.strokeRect(image_x-border_padding, image_y-border_padding,
                   image.width+2*border_padding, image.height+2*border_padding);

    // Draw captions.
    ctx.fillStyle = "white";
    ctx.textAlign = "center";
    ctx.font = caption_font(caption_size);
    var caption_y = image_y+image.height + border_padding + 1.5*caption_padding + caption_size;
    ctx.fillText(caption, canvas.width/2, caption_y);
    ctx.font = subcaption_font(subcaption_size);
    for (var i = 0; i < subcaption_lines.length; i++) {
      ctx.fillText(subcaption_lines[i], canvas.width/2,
                   caption_y + subcaption_padding + subcaption_size +
                   subcaption_size*i + subcaption_line_padding*(i+1));
    }
  }
}

function radio_input_type_click(event)
{
  switch (event.target.value) {
  case "url":
    document.getElementById("image_url").removeAttribute("disabled");
    document.getElementById("image_file").setAttribute("disabled", true);
    break;
  case "file":
    document.getElementById("image_file").removeAttribute("disabled");
    document.getElementById("image_url").setAttribute("disabled", true);
    break;
  default:
    alert("I'm trying to handle an invalid event.\nThis shouldn't ever happen.");
    break;
  }    
}
